require "html-proofer"

task :default => [:test]

desc "Run HTMLProofer"
task :test do
  cd "example" do
    sh "jekyll build"
    options = { empty_alt_ignore: true, enforce_https: true }
    HTMLProofer.check_directory("./_site", options).run
  end
end
